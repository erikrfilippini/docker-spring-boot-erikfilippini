FROM openjdk:12
ADD target/docker-spring-boot-erikfilippini.jar docker-spring-boot-erikfilippini.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-erikfilippini.jar"]
